# NosDrive

Experimental Google Drive relay.

Users sign in and connect their pubkey to Google Drive.
Then they add the relay to their relay list.

When they post EVENTs to the relay, it creates a file for that event in their Drive.

This is a write-only relay. REQs always EOSE.
