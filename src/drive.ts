import { NostrEvent } from '@nostrify/nostrify';
import { drive_v3 } from 'googleapis';

/**
 * Upload the Nostr event to Google Drive.
 *
 * Unfortunately Google Drive does not let you overwrite files by filename,
 * so we have to check if the file exists before uploading.
 */
export async function uploadEvent(drive: drive_v3.Drive, event: NostrEvent): Promise<void> {
  const directory = await getOrCreateDirectory(drive, 'nostr');

  await getOrCreateFile(
    drive,
    `${event.id}.json`,
    JSON.stringify(event),
    'application/json',
    directory,
  );
}

/** Get or create a directory with the given name. */
async function getOrCreateDirectory(
  drive: drive_v3.Drive,
  name: string,
): Promise<drive_v3.Schema$File> {
  const res = await drive.files.list({
    q: `mimeType='application/vnd.google-apps.folder' and name='${name}'`,
    fields: 'nextPageToken, files(id, name)',
  });

  if (res.data.files?.length) {
    return res.data.files[0];
  }

  const response = await drive.files.create({
    requestBody: {
      name: name,
      mimeType: 'application/vnd.google-apps.folder',
    },
    fields: 'id',
  });

  return response.data;
}

/** Get or create a file with the given name. */
async function getOrCreateFile(
  drive: drive_v3.Drive,
  name: string,
  content: string,
  mimeType: string,
  parent: drive_v3.Schema$File,
): Promise<drive_v3.Schema$File> {
  const res = await drive.files.list({
    q: `name='${name}' and '${parent.id}' in parents`,
    fields: 'nextPageToken, files(id, name)',
  });

  if (res.data.files?.length) {
    return res.data.files[0];
  }

  const response = await drive.files.create({
    requestBody: {
      name: name,
      parents: [parent.id!],
    },
    media: {
      mimeType,
      body: content,
    },
    fields: 'id',
  });

  return response.data;
}
