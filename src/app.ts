import { Hono } from 'hono';

import { AppEnv } from '@/app/AppEnv.ts';
import { connectController } from '@/controllers/connectController.ts';
import { loginController } from '@/controllers/loginController.ts';
import { logoutController } from '@/controllers/logoutController.ts';
import { oauthCallbackController } from '@/controllers/oauthCallbackController.ts';
import { relayController } from '@/controllers/relayController.ts';
import { userinfoController } from '@/controllers/userinfoController.ts';
import { pageHandler } from '@/handlers/pageHandler.tsx';
import { credentialsMiddleware } from '@/middleware/credentialsMiddleware.ts';
import { googleIdMiddleware } from '@/middleware/googleIdMiddleware.ts';
import { oauth2Middleware } from '@/middleware/oauth2Middleware.ts';
import { pubkeyMiddleware } from '@/middleware/pubkeyMiddleware.ts';
import { requireLoginMiddleware } from '@/middleware/requireLoginMiddleware.ts';
import { sessionMiddleware } from '@/middleware/sessionMiddleware.ts';
import { userinfoMiddleware } from '@/middleware/userinfoMiddleware.ts';
import { ConnectForm } from '@/views/ConnectForm.tsx';
import { Homepage } from '@/views/Homepage.tsx';
import { disconnectController } from '@/controllers/disconnectController.ts';

const app = new Hono<AppEnv>();

app.get('/relay', relayController);

app.use(
  '*',
  sessionMiddleware,
  googleIdMiddleware,
  credentialsMiddleware,
  oauth2Middleware,
  pubkeyMiddleware,
  userinfoMiddleware,
);

app.get('/', pageHandler(Homepage));
app.get('/login', loginController);
app.get('/logout', logoutController);
app.get('/userinfo', userinfoController);
app.get('/oauth/callback', oauthCallbackController);
app.get('/connect', requireLoginMiddleware, pageHandler(ConnectForm));
app.get('/disconnect', disconnectController);
app.post('/connect', connectController);

export default app;
