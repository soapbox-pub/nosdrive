/** @jsx jsx */

import { FC, jsx } from 'hono/middleware';

import { Button } from '@/views/components/Button.tsx';
import { GoogleIcon } from '@/views/components/GoogleIcon.tsx';

export const SignInButton: FC = () => {
  return (
    <Button href='/login'>
      <span class='flex items-center space-x-2'>
        <GoogleIcon />
        <span>Sign In</span>
      </span>
    </Button>
  );
};
