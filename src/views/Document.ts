import { html, raw } from 'hono/helper';
import { Child, FC } from 'hono/middleware';

import tailwindConfig from '@/views/tailwind.json' with { type: 'json' };

interface DocumentProps {
  title: string;
  children: Child;
}

export const Document: FC<DocumentProps> = ({ title, children }) => {
  const description = 'Your Nostr events on Google Drive.';

  return html`<!DOCTYPE html>
<html>
  <head>
    <title>${title}</title>
    <meta property="og:title" content="${title}"/>
    <meta name="twitter:title" content="${title}"/>
    <meta name="description" content="${description}"/>
    <meta property="og:description" content="${description}"/>
    <meta name="twitter:description" content="${description}"/>
    <script src="https://cdn.tailwindcss.com?plugins=typography"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap" rel="stylesheet">
    <script>tailwind.config = ${raw(JSON.stringify(tailwindConfig))};</script>
    <link rel="icon" type="image/png" href="https://favi.deno.dev/%F0%9F%93%8E.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
  </head>
  <body>
    ${children}
  </body>
</html>`;
};
