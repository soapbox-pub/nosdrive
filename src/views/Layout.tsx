/** @jsx jsx */
import { Child, FC, jsx } from 'hono/middleware';

import { AppContext } from '@/app/AppContext.ts';
import { Title } from '@/views/components/Title.tsx';
import { NavBar } from '@/views/components/NavBar.tsx';
import { SignInButton } from '@/views/SignInButton.tsx';
import { UserIndicator } from '@/views/UserIndicator.tsx';

interface LayoutProps {
  c: AppContext;
  children: Child;
}

export const Layout: FC<LayoutProps> = ({ c, children }) => {
  const userinfo = c.get('userinfo');

  return (
    <div class='p-4 max-w-prose mx-auto mb-36'>
      <NavBar>
        {c.req.path !== '/' && <a href='/' class='mr-auto'>Back</a>}
        {userinfo ? <UserIndicator userinfo={userinfo} /> : <SignInButton />}
      </NavBar>

      <div class='my-16 sm:my-24 md:my-36 text-center space-y-3'>
        <Title>
          <span class='bg-clip-text text-transparent bg-gradient-to-r from-lime-600 to-sky-500'>Nos</span>
          <span class='bg-clip-text text-transparent bg-gradient-to-r from-yellow-500 to-amber-600'>Drive</span>
        </Title>
        <p class='text-2xl'>Your Nostr events on Google Drive</p>
      </div>

      {children}
    </div>
  );
};
