/** @jsx jsx */

import { oauth2_v2 } from 'googleapis';
import { FC, jsx } from 'hono/middleware';

interface UserIndicatorProps {
  userinfo: oauth2_v2.Schema$Userinfo;
}

export const UserIndicator: FC<UserIndicatorProps> = ({ userinfo }) => {
  return (
    <div class='flex space-x-4 items-center'>
      <a href='/logout'>Log out</a>
      <img class='w-10 h-10 rounded-full bg-gray-300' src={userinfo.picture!} alt={userinfo.name ?? ''} />
    </div>
  );
};
