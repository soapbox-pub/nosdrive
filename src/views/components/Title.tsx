/** @jsx jsx */

import { FC, jsx } from 'hono/middleware';

export const Title: FC = ({ children }) => {
  return (
    <h1 class='text-3xl sm:text-6xl md:text-7xl font-extrabold leading-tight md:leading-none'>
      {children}
    </h1>
  );
};
