/** @jsx jsx */

import { FC, jsx } from 'hono/middleware';

export const NavBar: FC = ({ children }) => {
  return (
    <nav class='flex justify-end items-center space-x-4'>
      {children}
    </nav>
  );
};
