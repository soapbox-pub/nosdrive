/** @jsx jsx */

import { FC, jsx } from 'hono/middleware';

interface InputProps {
  type: 'text';
  name?: string;
  value?: string;
  placeholder?: string;
  readonly?: boolean;
}

export const Input: FC<InputProps> = ({ type, name, value, placeholder, readonly }) => {
  return (
    <div class='bg-black/5 flex justify-between py-1.5 px-3 rounded-2xl text-lg border border-black/10 focus-within:border-black/50 backdrop-blur'>
      <input
        class='bg-transparent text-black placeholder-black/50 w-full focus:outline-none px-2 h-10'
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        readonly={readonly}
      />
    </div>
  );
};
