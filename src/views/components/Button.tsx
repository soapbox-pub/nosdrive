/** @jsx jsx */

import { Child, FC, jsx } from 'hono/middleware';

interface ButtonProps {
  children: Child;
  href?: string;
}

export const Button: FC<ButtonProps> = ({ href, children }) => {
  const Comp = href ? 'a' : 'button';

  return (
    <Comp
      class='py-3 px-5 bg-red-600 hover:bg-red-700 focus:ring ring-amber-400 text-lg text-white font-bold transition rounded-full'
      href={href}
    >
      {children}
    </Comp>
  );
};
