/** @jsx jsx */

import { jsx } from 'hono/middleware';
import { nip19 } from 'nostr-tools';

import { AppPage } from '@/app/AppPage.ts';
import { Button } from '@/views/components/Button.tsx';
import { Input } from '@/views/components/Input.tsx';

export const ConnectForm: AppPage = ({ c }) => {
  const pubkey = c.get('pubkey');

  return (
    <div class='space-y-16'>
      <h1 class='text-center text-3xl'>Drive + Nostr</h1>
      <form method='post' action='/connect' class='flex flex-col space-y-4'>
        <p>Enter your npub</p>

        <Input
          type='text'
          name='npub'
          value={pubkey ? nip19.npubEncode(pubkey) : ''}
          placeholder='npub1...'
        />

        <Button>Submit</Button>
      </form>

      <div class='text-center'>
        <a href='/disconnect' class='text-red-700'>Disconnect</a>
      </div>
    </div>
  );
};
