/** @jsx jsx */

import { jsx } from 'hono/middleware';
import { nip19 } from 'nostr-tools';

import { AppPage } from '@/app/AppPage.ts';
import { Conf } from '@/conf.ts';
import { Input } from '@/views/components/Input.tsx';
import { NostrIcon } from '@/views/components/NostrIcon.tsx';
import { Button } from '@/views/components/Button.tsx';

export const Homepage: AppPage = ({ c }) => {
  const userinfo = c.get('userinfo');
  const pubkey = c.get('pubkey');

  return (
    <div class='text-center space-y-36'>
      {(userinfo && pubkey) && (
        <div class='space-y-12'>
          <h1 class='text-4xl'>Connected!</h1>

          <div class='flex -space-x-4 justify-center'>
            <div class='flex bg-white rounded-full shadow-lg w-32 h-32 p-8 items-center justify-center'>
              <img src='https://duckduckgo.com/i/509a9ea591804d6f.png' />
            </div>
            <div class='flex bg-white rounded-full shadow-lg w-32 h-32 p-8 items-center justify-center text-violet-700'>
              <NostrIcon class='w-full h-full p-1' />
            </div>
          </div>

          <p>
            Your account <span class='font-bold'>{userinfo.name}</span> is connected to
            <code class='font-mono truncate overflow-hidden inline-block max-w-full'>{nip19.npubEncode(pubkey)}</code>
            on NosDrive. <a class='text-red-700' href='/connect'>Edit</a>
          </p>
        </div>
      )}

      {(userinfo && !pubkey) && (
        <div class='space-y-12'>
          <h1 class='text-4xl'>Almost there...</h1>

          <p>
            You need to <span class='font-bold'>add your npub</span> to finish setting up NosDrive.
          </p>

          <div>
            <Button href='/connect'>Connect</Button>
          </div>
        </div>
      )}

      <div class='space-y-8'>
        <h2 class='text-3xl'>Relay</h2>
        <div class='space-y-4'>
          <Input type='text' value={Conf.relayUrl} readonly />
          <p class='text-gray-700 text-sm'>
            You <span class='font-bold'>must</span> add the relay to your client for NosDrive to save your events.
          </p>
        </div>
      </div>

      <div class='space-y-8 text-left'>
        <h2 class='text-3xl'>FAQ</h2>
        <div class='space-y-16 prose'>
          <div>
            <h3 class='text-2xl'>What is NosDrive?</h3>
            <p>
              NosDrive is a <span class='font-bold'>Nostr Relay</span> that saves your events to Google Drive.
            </p>
          </div>
          <div>
            <h3 class='text-2xl'>Why would I want that?</h3>
            <p>
              <b>tl;dr</b> To keep a back-up of all your data on Nostr.
            </p>
            <p>
              Nostr events are completely portable, because they're digitally signed. If you have a copy of your events,
              you can manipulate them, import them into Nostr apps, and restore them to relays in case of failure.
            </p>
          </div>
          <div>
            <h3 class='text-2xl'>Isn't Google bad?</h3>
            <p>
              Yes, of course. But it lowers the barrier for users to have direct access to their Nostr events. It's free
              real-estate!
            </p>
            <p>
              NosDrive is a secondary service for backup/archival purposes. It does not increase reliance on big tech.
            </p>
          </div>
          <div>
            <h3 class='text-2xl'>How do I set it up?</h3>
            <div>
              <ol>
                <li>
                  <b>Sign in</b> to NosDrive with your Google account.
                </li>
                <li>
                  Enter your npub into the form and click <b>Submit</b>.
                </li>
                <li>
                  Copy the <b>Relay URL</b> and add it to your Nostr client.
                </li>
                <li>Start making new posts on Nostr.</li>
                <li>Check your Google Drive. You will see a new folder called "nostr" with your events!</li>
              </ol>
            </div>
          </div>
          <div>
            <h3 class='text-2xl'>How do I disconnect?</h3>
            <p>
              You can disconnect from NosDrive by revoking access in your Google{' '}
              <a href='https://myaccount.google.com/connections' target='_blank'>account settings</a>.
            </p>
          </div>
          <div>
            <h3 class='text-2xl'>Who made this?</h3>
            <p>
              This project was created by Alex Gleason as part of the efforts behind{' '}
              <a href='https://soapbox.pub/' target='_blank'>Soapbox</a>.
            </p>
            <p>
              NosDrive is{' '}
              <a href='https://gitlab.com/soapbox-pub/nosdrive' target='_blank'>open source</a>. Contributions are
              welcome. If you think you found a bug, please{' '}
              <a href='https://gitlab.com/soapbox-pub/nosdrive/-/issues' target='_blank'>report it!</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
