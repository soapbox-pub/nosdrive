import '@std/dotenv/load';
import app from '@/app.ts';

Deno.serve(app.fetch);
