import { Handler } from 'hono';

import { AppEnv } from '@/app/AppEnv.ts';

export type AppController = Handler<AppEnv>;
