import { Credentials, OAuth2Client } from 'google-auth-library';
import { oauth2_v2 } from 'googleapis';
import { Env as HonoEnv } from 'hono';

export interface AppEnv extends HonoEnv {
  Variables: {
    session?: string | false;
    googleId?: string | null;
    credentials?: Credentials | null;
    oauth2?: OAuth2Client;
    userinfo?: oauth2_v2.Schema$Userinfo;
    pubkey?: string;
  };
}
