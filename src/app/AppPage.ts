import { FC } from 'hono/middleware';

import { AppContext } from '@/app/AppContext.ts';

export type AppPage = FC<{ c: AppContext }>;
