import { MiddlewareHandler } from 'hono';

import { AppEnv } from '@/app/AppEnv.ts';

export type AppMiddleware = MiddlewareHandler<AppEnv>;
