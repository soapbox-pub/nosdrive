import { Context as HonoContext } from 'hono';

import { AppEnv } from '@/app/AppEnv.ts';

export type AppContext = HonoContext<AppEnv>;
