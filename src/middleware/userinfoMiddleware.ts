import { google } from 'googleapis';

import { AppMiddleware } from '@/app/AppMiddleware.ts';

export const userinfoMiddleware: AppMiddleware = async (c, next) => {
  const auth = c.get('oauth2');

  if (auth) {
    const client = google.oauth2({ version: 'v2', auth });
    const { data: userinfo } = await client.userinfo.get();

    c.set('userinfo', userinfo);
  }

  await next();
};
