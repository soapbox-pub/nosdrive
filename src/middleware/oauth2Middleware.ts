import { AppMiddleware } from '@/app/AppMiddleware.ts';
import { getOAuthClient } from '@/auth.ts';

export const oauth2Middleware: AppMiddleware = async (c, next) => {
  const credentials = c.get('credentials');

  if (credentials) {
    const auth = getOAuthClient();
    auth.setCredentials(credentials);
    c.set('oauth2', auth);
  }

  await next();
};
