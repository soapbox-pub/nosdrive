import { AppMiddleware } from '@/app/AppMiddleware.ts';
import { getGoogleIdBySession } from '@/db.ts';

export const googleIdMiddleware: AppMiddleware = async (c, next) => {
  const session = c.get('session');

  if (session) {
    const googleId = await getGoogleIdBySession(session);
    c.set('googleId', googleId);
  }

  await next();
};
