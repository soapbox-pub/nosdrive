import { AppMiddleware } from '@/app/AppMiddleware.ts';

export const requireLoginMiddleware: AppMiddleware = async (c, next) => {
  const userinfo = c.get('userinfo');

  if (!userinfo) {
    return c.redirect('/login');
  }

  await next();
};
