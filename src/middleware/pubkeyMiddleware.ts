import { AppMiddleware } from '@/app/AppMiddleware.ts';
import { getPubkeyByGoogleId } from '@/db.ts';

export const pubkeyMiddleware: AppMiddleware = async (c, next) => {
  const googleId = c.get('googleId');

  if (googleId) {
    const pubkey = await getPubkeyByGoogleId(googleId);
    c.set('pubkey', pubkey ?? undefined);
  }

  await next();
};
