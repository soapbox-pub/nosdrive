import { AppMiddleware } from '@/app/AppMiddleware.ts';
import { getOrRefreshCredentials } from '@/auth.ts';

export const credentialsMiddleware: AppMiddleware = async (c, next) => {
  const googleId = c.get('googleId');

  if (googleId) {
    const credentials = await getOrRefreshCredentials(googleId);
    c.set('credentials', credentials);
  }

  await next();
};
