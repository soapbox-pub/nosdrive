import { getSignedCookie } from 'hono/helper';

import { AppMiddleware } from '@/app/AppMiddleware.ts';
import { Conf } from '@/conf.ts';

export const sessionMiddleware: AppMiddleware = async (c, next) => {
  const session = await getSignedCookie(c, Conf.secretKey, 'session', 'host');
  c.set('session', session);

  await next();
};
