/** @jsx jsx */

import { jsx } from 'hono/middleware';

import { AppPage } from '@/app/AppPage.ts';
import { AppController } from '@/app/AppController.ts';
import { Document } from '@/views/Document.ts';
import { Layout } from '@/views/Layout.tsx';

export function pageHandler(Comp: AppPage, title?: string): AppController {
  return (c) =>
    c.html(
      <Document title={title ? `NosDrive | ${title}` : 'NosDrive'}>
        <Layout c={c}>
          <Comp c={c} />
        </Layout>
      </Document>,
    );
}
