import { encodeBase64 } from '@std/encoding/base64';
import { google } from 'googleapis';
import { setSignedCookie } from 'hono/helper';

import { AppController } from '@/app/AppController.ts';
import { getOAuthClient } from '@/auth.ts';
import { Conf } from '@/conf.ts';
import { setGoogleCredentials, setSession } from '@/db.ts';

export const oauthCallbackController: AppController = async (c) => {
  const code = c.req.query('code');
  if (!code) {
    return c.json({ error: 'No code provided' }, 400);
  }

  const auth = getOAuthClient();
  const { tokens } = await auth.getToken(code);
  auth.setCredentials(tokens);

  const client = google.oauth2({ version: 'v2', auth });
  const { data: userinfo } = await client.userinfo.get();

  if (!userinfo.id) {
    return c.text('No user ID found', 400);
  }

  // Save token
  await setGoogleCredentials(userinfo.id, tokens);

  // Generate session
  const session = encodeBase64(crypto.getRandomValues(new Uint8Array(32)));
  await setSession(userinfo.id, session);
  await setSignedCookie(c, 'session', session, Conf.secretKey, {
    httpOnly: true,
    secure: true,
    prefix: 'host',
    sameSite: 'Lax',
    path: '/',
  });

  return c.redirect('/');
};
