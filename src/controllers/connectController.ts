import { AppController } from '@/app/AppController.ts';
import { setConnection } from '@/db.ts';
import { nip19 } from 'nostr-tools';

export const connectController: AppController = async (c) => {
  const googleId = c.get('googleId');

  if (!googleId) {
    return c.text('Unauthorized', 401);
  }

  const formData = await c.req.formData();
  const npub = formData.get('npub');

  if (!npub) {
    return c.text('No npub provided', 400);
  }

  let pubkey: string | undefined;

  try {
    const result = nip19.decode(npub as string);
    if (result.type === 'npub') {
      pubkey = result.data;
    }
  } catch (_e) {
    return c.text('Invalid npub', 400);
  }

  if (!pubkey) {
    return c.json({ error: 'No pubkey provided' }, 400);
  }

  await setConnection({ pubkey, googleId });

  return c.redirect('/');
};
