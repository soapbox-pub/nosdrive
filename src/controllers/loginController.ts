import { AppController } from '@/app/AppController.ts';
import { getAuthUrl } from '@/auth.ts';

export const loginController: AppController = (c) => {
  const userinfo = c.get('userinfo');

  if (!userinfo) {
    return c.redirect(getAuthUrl());
  }

  return c.redirect('/');
};
