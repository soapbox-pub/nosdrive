import { Handler } from 'hono';

import { Conf } from '@/conf.ts';

const relayInfoController: Handler = (c) => {
  return c.json({
    name: 'NosDrive',
    description: '',
    pubkey: Conf.contactPubkey,
    contact: Conf.contactUri,
    supported_nips: [],
    software: 'NosDrive',
    version: '0.0.0',
    limitation: {
      max_subscriptions: 0,
      max_filters: 0,
      max_limit: 0,
      max_subid_length: 0,
      auth_required: false,
      payment_required: false,
      restricted_writes: true,
    },
  });
};

export { relayInfoController };
