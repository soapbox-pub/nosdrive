import { deleteCookie } from 'hono/helper';

import { AppController } from '@/app/AppController.ts';
import { Conf } from '@/conf.ts';

export const logoutController: AppController = (c) => {
  deleteCookie(c, 'session', {
    httpOnly: true,
    secure: true,
    prefix: 'host',
    sameSite: 'Strict',
    signingSecret: Conf.secretKey,
  });

  return c.redirect('/');
};
