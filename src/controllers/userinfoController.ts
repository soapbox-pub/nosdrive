import { AppController } from '@/app/AppController.ts';

export const userinfoController: AppController = (c) => {
  const userinfo = c.get('userinfo');
  const pubkey = c.get('pubkey');
  return c.json({ ...userinfo, pubkey });
};
