import { AppController } from '@/app/AppController.ts';
import { removeConnection } from '@/db.ts';

export const disconnectController: AppController = async (c) => {
  const googleId = c.get('googleId');

  if (googleId) {
    await removeConnection(googleId);
  }

  return c.redirect('/');
};
