import {
  NostrClientCOUNT,
  NostrClientEVENT,
  NostrClientMsg,
  NostrClientREQ,
  NostrEvent,
  NostrRelayMsg,
  NSchema as n,
} from '@nostrify/nostrify';
import { google } from 'googleapis';
import { Handler } from 'hono';
import { verifyEvent } from 'nostr-tools';

import { getOAuthClient, getOrRefreshCredentials } from '@/auth.ts';
import { relayInfoController } from '@/controllers/relayInfoController.ts';
import { listGoogleIdsByPubkey } from '@/db.ts';
import { uploadEvent } from '@/drive.ts';

/** Set up the Websocket connection. */
function connectStream(socket: WebSocket) {
  socket.onmessage = (e) => {
    const result = n.json().pipe(n.clientMsg()).safeParse(e.data);
    if (result.success) {
      handleMsg(result.data);
    } else {
      send(['NOTICE', 'Invalid message.']);
    }
  };

  /** Handle client message. */
  function handleMsg(msg: NostrClientMsg) {
    switch (msg[0]) {
      case 'REQ':
        handleReq(msg);
        return;
      case 'EVENT':
        handleEvent(msg);
        return;
      case 'COUNT':
        handleCount(msg);
        return;
    }
  }

  /** Handle REQ. Nothing to do. */
  function handleReq([_, subId]: NostrClientREQ): void {
    send(['EOSE', subId]);
    send(['CLOSED', subId, 'write-only relay']);
  }

  /** Handle EVENT. Upload the event to Google Drive. */
  async function handleEvent([_, event]: NostrClientEVENT): Promise<void> {
    if (!verifyEvent(event)) {
      send(['OK', event.id, false, 'invalid: event signature']);
      return;
    }

    const results = await uploadDrive(event);

    if (results.success.length) {
      send(['OK', event.id, true, '']);
      return;
    }

    if (!results.success.length && !results.failed.length) {
      send(['OK', event.id, false, 'error: no Google accounts connected']);
      return;
    }

    send(['OK', event.id, false, 'error: failed to upload to Google Drive']);
  }

  async function uploadDrive(event: NostrEvent): Promise<{ success: string[]; failed: [string, Error][] }> {
    const success: string[] = [];
    const failed: [string, Error][] = [];

    for await (const googleId of listGoogleIdsByPubkey(event.pubkey)) {
      try {
        const credentials = await getOrRefreshCredentials(googleId);
        if (!credentials) throw new Error('NosDrive: try logging in and reconnect to Google');
        const auth = getOAuthClient();
        auth.setCredentials(credentials);
        const drive = google.drive({ version: 'v3', auth });
        await uploadEvent(drive, event);
        success.push(googleId);
      } catch (e) {
        failed.push([googleId, e]);
      }
    }

    return { success, failed };
  }

  /** Handle COUNT. Return 0. */
  function handleCount([_, subId]: NostrClientCOUNT): void {
    send(['COUNT', subId, { count: 0, approximate: false }]);
  }

  /** Send a message back to the client. */
  function send(msg: NostrRelayMsg): void {
    if (socket.readyState === WebSocket.OPEN) {
      socket.send(JSON.stringify(msg));
    }
  }
}

export const relayController: Handler = (c, next) => {
  const upgrade = c.req.header('upgrade');

  // NIP-11: https://github.com/nostr-protocol/nips/blob/master/11.md
  if (c.req.header('accept') === 'application/nostr+json') {
    return relayInfoController(c, next);
  }

  if (upgrade?.toLowerCase() !== 'websocket') {
    return c.text('Please use a Nostr client to connect.', 400);
  }

  const { socket, response } = Deno.upgradeWebSocket(c.req.raw);
  connectStream(socket);

  return response;
};
