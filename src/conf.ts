export class Conf {
  static get secretKey(): string {
    const value = Deno.env.get('SECRET_KEY');
    if (!value) {
      throw new Error('SECRET_KEY is not set. Generate one with `openssl rand -base64 48`');
    }
    return value;
  }

  static get relayUrl(): string {
    return Deno.env.get('RELAY_URL') ?? 'wss://nosdrive.app/relay';
  }

  static get googleClientId(): string | undefined {
    return Deno.env.get('GOOGLE_CLIENT_ID');
  }

  static get googleClientSecret(): string | undefined {
    return Deno.env.get('GOOGLE_CLIENT_SECRET');
  }

  static get googleRedirectUri(): string | undefined {
    return Deno.env.get('GOOGLE_REDIRECT_URI');
  }

  static get contactPubkey(): string | undefined {
    return Deno.env.get('CONTACT_PUBKEY');
  }

  static get contactUri(): string | undefined {
    return Deno.env.get('CONTACT_URI');
  }
}
