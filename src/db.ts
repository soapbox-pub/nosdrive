import { Credentials } from 'google-auth-library';

const kv = await Deno.openKv();

interface GooglePubkey {
  googleId: string;
  pubkey: string;
}

export function setGoogleCredentials(googleId: string, credentials: Credentials): Promise<Deno.KvCommitResult> {
  return kv.set(['googleCredentials', googleId], credentials);
}

export async function getGoogleCredentials(googleId: string): Promise<Credentials | null> {
  const result = await kv.get<Credentials>(['googleCredentials', googleId]);
  return result?.value;
}

export function setSession(googleId: string, session: string): Promise<Deno.KvCommitResult> {
  return kv.set(['sessions', session], googleId);
}

export async function getGoogleIdBySession(session: string): Promise<string | null> {
  const result = await kv.get<string>(['sessions', session]);
  return result?.value;
}

/** Set a connection between a Google ID and a pubkey, replacing any existing associations. */
export async function setConnection(
  { googleId, pubkey }: GooglePubkey,
): Promise<Deno.KvCommitResult | Deno.KvCommitError> {
  const prevPubkey = await getPubkeyByGoogleId(googleId);
  const atomic = kv.atomic();

  if (prevPubkey) {
    atomic.delete(['googleIdsByPubkey', prevPubkey, googleId]);
  }

  return atomic
    .set(['pubkeysByGoogleId', googleId], pubkey)
    .set(['googleIdsByPubkey', pubkey, googleId], true)
    .commit();
}

export async function removeConnection(googleId: string): Promise<void> {
  const pubkey = await getPubkeyByGoogleId(googleId);
  if (!pubkey) return;

  await kv.atomic()
    .delete(['pubkeysByGoogleId', googleId])
    .delete(['googleIdsByPubkey', pubkey, googleId])
    .commit();
}

export async function getPubkeyByGoogleId(googleId: string): Promise<string | null> {
  const result = await kv.get<string>(['pubkeysByGoogleId', googleId]);
  return result?.value;
}

export async function* listGoogleIdsByPubkey(pubkey: string, opts?: Deno.KvListOptions): AsyncIterable<string> {
  for await (const { key } of kv.list<string>({ prefix: ['googleIdsByPubkey', pubkey] }, opts)) {
    yield key[key.length - 1] as string;
  }
}
