import { google } from 'googleapis';
import { Credentials, OAuth2Client } from 'google-auth-library';

import { Conf } from '@/conf.ts';
import { getGoogleCredentials, setGoogleCredentials } from '@/db.ts';

/** Google OAuth client. */
export function getOAuthClient(): OAuth2Client {
  return new google.auth.OAuth2(
    Conf.googleClientId,
    Conf.googleClientSecret,
    Conf.googleRedirectUri,
  );
}

/**
 * Generates an auth URL to display to users.
 *
 * After the user authorizes the app, they will be redirected to the `GOOGLE_REDIRECT_URI`,
 * with `code` and `scope` query params. Eg:
 *
 * ```txt
 * https://nosdrive.app/oauth/callback?code=1234567890abcdef&scope=https://www.googleapis.com/auth/drive.file
 * ```
 */
export function getAuthUrl(): string {
  return getOAuthClient().generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent',
    scope: [
      'https://www.googleapis.com/auth/drive.file',
      'https://www.googleapis.com/auth/userinfo.profile',
    ],
  });
}

/** Get the user's access token from the database, and refresh it before returning if it already expired. */
export async function getOrRefreshCredentials(googleId: string): Promise<Credentials | null> {
  const credentials = await getGoogleCredentials(googleId);

  if (!credentials) {
    return null;
  }

  const auth = getOAuthClient();
  auth.setCredentials(credentials);

  const expiry = credentials.expiry_date ?? 0;
  const now = Date.now();

  if (expiry - now < 0) {
    const result = await auth.refreshAccessToken();
    await setGoogleCredentials(googleId, result.credentials);
    return result.credentials;
  }

  return credentials;
}
